import java.awt.*;

public class Ball {
    private int x;
    private int y;
    private final int diameter;
    private int xSpeed;
    private int ySpeed;
    private final Color color;
    private int sizeBoardWidth;
    private int sizeBoardHeight;
    private Thread thread;

    public Ball(int x, int y, int diameter, int xSpeed, int ySpeed, Color color) {
        this.x = x;
        this.y = y;
        this.diameter = diameter;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
        this.color = color;
    }

    void go() {
        x = x + (xSpeed*25)/1000;
        y = y + (ySpeed*25)/1000;
        int maxX = sizeBoardWidth - diameter / 2;
        int maxY = sizeBoardHeight - diameter / 2;

        if( x < 0 ) {
            x = 0;
            xSpeed = -xSpeed;
        } else if( x > maxX ) {
            x = maxX;
            xSpeed = -xSpeed;
        } else if( y < 0 ) {
            y = 0;
            ySpeed = -ySpeed;
        } else if( y > maxY ) {
            y = maxY;
            ySpeed = -ySpeed;
        }
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public void setSizeBoardWidth(int sizeBoard) {
        this.sizeBoardWidth = sizeBoard;
    }

    public void setSizeBoardHeight(int sizeBoard) {
        this.sizeBoardHeight = sizeBoard;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDiameter() {
        return diameter;
    }

    public Color getColor() {
        return color;
    }

    public Thread getThread() {
        return thread;
    }
}
