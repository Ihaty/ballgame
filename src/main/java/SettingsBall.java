import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class SettingsBall {
    private final Random randomRGB;

    public SettingsBall() {
        randomRGB = new Random();
    }

    public Color randomColor() {
        int red = randomRGB.nextInt(255);
        int green = randomRGB.nextInt(255);
        int black = randomRGB.nextInt(255);
        Color color = new Color(red, green, black);
        return color;
    }

    public Integer direction() {
        return -180 + (int) (Math.random() * ((180 - (-180)) + 1));
    }

    public Integer diameter() {
        return (int) (Math.random() * 100);
    }
}
