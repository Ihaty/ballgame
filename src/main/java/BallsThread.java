import javax.swing.*;
import java.util.Random;

public class BallsThread extends JPanel implements Runnable{
    private final Ball ball;
    private final Board board;
    private final Random random = new Random();

    public BallsThread(Ball ball, Board board) {
       this.ball = ball;
       this.board = board;
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep( 25 );
                ball.go();
                board.repaint();
                paintComponent(board.getGraphics());
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
