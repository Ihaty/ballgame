import javax.swing.*;
import java.awt.*;

public class DrawingCanvas extends JFrame {

    public DrawingCanvas() {
        setContentPane(new Board());
        setResizable(false);
        setSize(new Dimension(750, 600));
        setTitle("Balls");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }
}