import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board extends JPanel {
    private Ball ball;
    private Board board;
    private final SettingsBall settingsBall;
    private List<Ball> listBalls;

    public Board() {
        listBalls = new ArrayList<>();
        settingsBall = new SettingsBall();
        board = this;
        setBackground(Color.BLACK);
        createBall();
    }

    private void createBall() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (removeBall(e)) {
                    super.mousePressed(e);
                    ball = new Ball(e.getX(), e.getY(), settingsBall.diameter(), settingsBall.direction(),
                            settingsBall.direction(), settingsBall.randomColor());
                    ball.setSizeBoardWidth(getWidth());
                    ball.setSizeBoardHeight(getHeight());
                    listBalls.add(ball);
                    Thread thread = new Thread(new BallsThread(ball, board));
                    ball.setThread(thread);
                    thread.start();
                }
            }
        });
    }

    private boolean removeBall(MouseEvent e) {
        for (Ball ball : listBalls) {
            double s = Point.distance(ball.getX(), ball.getY(), e.getX(), e.getY());
            if (s < ball.getDiameter()) {
                listBalls.remove(ball);
                ball.getThread().interrupt();
                board.repaint();
                return false;
            }
        }
        return true;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Ball b : listBalls) {
            g.setColor(b.getColor());
            g.fillOval(b.getX() - b.getDiameter() / 2, b.getY() - b.getDiameter() / 2, b.getDiameter(), b.getDiameter());
        }
    }
}